package com.thank.demo.controller;

import com.thank.demo.service.DemoService;
import com.thank.framework.annotaions.MyAutowired;
import com.thank.framework.annotaions.MyController;
import com.thank.framework.annotaions.MyRequestMapping;
import com.thank.framework.annotaions.MySecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * description: none
 *
 * @author thank
 * 2021/5/11 0:13
 */
@MyController
@MyRequestMapping("/demo")
public class DemoController {

    @MyAutowired
    private DemoService demoService;

    /**
     * URL: http://localhost:8080/demo/get?name=lisi
     */
    @MySecurity(value = {"admin", "thank"})
    @MyRequestMapping("/get")
    public String get(HttpServletRequest request, HttpServletResponse response, String username) throws IOException {
        System.out.println("Controller接受到参数name: " + username);
        response.getWriter().write("Hello ~" + demoService.get(username));
        return demoService.get(username);
    }
}
