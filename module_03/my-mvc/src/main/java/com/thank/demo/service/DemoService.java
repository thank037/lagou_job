package com.thank.demo.service;

/**
 * description: none
 *
 * @author thank
 * 2021/5/11 0:14
 */
public interface DemoService {

    String get(String name);
}
