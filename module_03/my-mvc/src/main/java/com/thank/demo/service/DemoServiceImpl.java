package com.thank.demo.service;

import com.thank.framework.annotaions.MyService;

/**
 * description: none
 *
 * @author thank
 * 2021/5/11 0:14
 */
@MyService("demoService")
public class DemoServiceImpl implements DemoService {
    @Override
    public String get(String name) {
        System.out.println("service 实现类中的name参数：" + name) ;
        return name;
    }
}
