package com.thank.framework.annotaions;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/12 13:45
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MySecurity {

    String[] value() default {};
}
