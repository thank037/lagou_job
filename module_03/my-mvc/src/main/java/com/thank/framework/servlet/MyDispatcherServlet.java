package com.thank.framework.servlet;

import com.thank.framework.annotaions.*;
import com.thank.framework.pojo.Handler;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * description: none
 *
 * @author thank
 * 2021/5/10 18:43
 */
public class MyDispatcherServlet extends HttpServlet {


    private List<String> classNameList = new ArrayList<>();

    private Map<String, Object> iocMap = new HashMap<>();

    private List<Handler> handlerMapping = new ArrayList<>();

    @Override
    public void init(ServletConfig config) throws ServletException {

        // 1. 加载spring-mvc.properties, 获取包的扫描路径
        String location = config.getInitParameter("contextConfigLocation");
        Properties properties = this.getProperties(location);
        String componentScan = String.valueOf(properties.get("component-scan"));

        // 2. 扫描指定路径下的所有Java类, 放入classNameList中
        this.doScan(componentScan);

        // 3. 实例化Bean对象(基于注解)
        this.doInstance();

        // 4. 依赖注入
        this.doAutowired();

        // 5. 构造一个HandlerMapping处理器映射器，将配置好的url和Method建立映射关系
        this.initHandlerMapping();

        System.out.println("自定义mini mvc 初始化完成....");
        System.out.println("handler数量" + handlerMapping.size());
    }

    private void initHandlerMapping() {
        if (iocMap.isEmpty()) {
            return;
        }

        for (Map.Entry<String, Object> entry : iocMap.entrySet()) {
            Class<?> clazz = entry.getValue().getClass();
            if (!clazz.isAnnotationPresent(MyRequestMapping.class)) {
                continue;
            }

            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                if (!method.isAnnotationPresent(MyRequestMapping.class)) {
                    continue;
                }
                String url = clazz.getAnnotation(MyRequestMapping.class).value() + method.getAnnotation(MyRequestMapping.class).value();
                Handler handler = new Handler(entry.getValue(), method, Pattern.compile(url));

                // 获取方法参数和位置
                Parameter[] parameters = method.getParameters();
                for (int i = 0; i < parameters.length; i++) {
                    Parameter parameter = parameters[i];
                    // 如果是request和response对象，那么参数名称写HttpServletRequest和HttpServletResponse
                    if(parameter.getType() == HttpServletRequest.class || parameter.getType() == HttpServletResponse.class) {
                        handler.getParamNameToIndexMapping().put(parameter.getType().getSimpleName(), i);
                    }else{
                        handler.getParamNameToIndexMapping().put(parameter.getName(), i);
                    }
                }

                handlerMapping.add(handler);
            }
        }
    }

    private void doAutowired() {

        if (iocMap.isEmpty()) {
            return;
        }

        for (Map.Entry<String, Object> entry : iocMap.entrySet()) {
            Object obj = entry.getValue();
            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field : fields) {
                MyAutowired annotation = field.getAnnotation(MyAutowired.class);
                if (annotation == null) {
                    continue;
                }

                String annotationValue = annotation.value();
                String beanId = "".equals(annotationValue) ? field.getType().getName() : annotationValue;
                field.setAccessible(true);
                try {
                    field.set(obj, iocMap.get(beanId));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void doInstance() {

        if (classNameList.size() == 0) {
            return;
        }

        try {
            for (String className : classNameList) {
                Class<?> clazz = Class.forName(className);
                String simpleName = clazz.getSimpleName();
                if (clazz.isAnnotationPresent(MyController.class)) {
                    // controller一般不会被用于设定value值后属性注入, 所以这里不考虑注解的value值
                    String beanId = this.lowercaseFirstLetter(simpleName);
                    iocMap.put(beanId, clazz.newInstance());
                } else if (clazz.isAnnotationPresent(MyService.class)) {
                    MyService annotation = clazz.getAnnotation(MyService.class);
                    String annotationValue = annotation.value();

                    String beanId = "".equals(annotationValue) ? lowercaseFirstLetter(simpleName) : annotationValue;
                    iocMap.put(beanId, clazz.newInstance());

                    // service层往往是有接口的，面向接口开发，此时再以接口的全限定类名为id，放入一份对象到ioc中，便于后期根据接口类型注入
                    Class<?>[] interfaces = clazz.getInterfaces();
                    if (interfaces.length > 0) {
                        for (Class<?> interfaceClazz : interfaces) {
                            iocMap.put(interfaceClazz.getName(), clazz.newInstance());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * 根据路径扫描所有class (递归)
     * @param componentScan 扫描包路径 e.g. 'com.thank'
     */
    private void doScan(String componentScan) {
        // 拿到本地磁盘的classes路径 + 扫描路径
        String scanFilePath = Thread.currentThread().getContextClassLoader().getResource("").getPath()
                + componentScan.replace(".", File.separator);
        File dir = new File(scanFilePath);
        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            return;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                doScan(componentScan + "." + file.getName());
            } else if (file.getName().endsWith(".class")) {
                String className = componentScan + "." + file.getName().replace(".class", "");
                classNameList.add(className);
            }
        }
    }

    private Handler getHandler(HttpServletRequest request) {
        if (handlerMapping.isEmpty()) {
            return null;
        }
        String uri = request.getRequestURI();
        for (Handler handler : handlerMapping) {
            Matcher matcher = handler.getUrlPattern().matcher(uri);
            if (matcher.matches()) {
                return handler;
            }
        }
        return null;
    }

    private Properties getProperties(String location) {
        Properties properties = new Properties();
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(location);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties;
    }

    /**
     * 首字母转小写
     */
    private String lowercaseFirstLetter(String str) {
        if (str.charAt(0) >= 97) {
            return str;
        }

        return (char) (str.charAt(0) + 32) + str.substring(1);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Handler handler = this.getHandler(req);
        if (null == handler) {
            resp.getWriter().write("404 Not found...");
            return;
        }

        String username = req.getParameter("username");
        if (!permissionCheck(handler, username)) {
            resp.getWriter().write("403 Forbidden...");
            return;
        }

        Object[] invokeArgs = new Object[handler.getMethod().getParameterCount()];

        Map<String, String[]> parameterMap = req.getParameterMap();
        Map<String, Integer> paramNameToIndexMapping = handler.getParamNameToIndexMapping();

        // 遍历request中所有参数  （填充除了request，response之外的参数）
        for (Map.Entry<String, String[]> param : parameterMap.entrySet()) {
            if (!paramNameToIndexMapping.containsKey(param.getKey())) {
                continue;
            }
            Integer index = paramNameToIndexMapping.get(param.getKey());
            // name=1&name=2   name [1,2] --> "1,2"
            String value = StringUtils.join(param.getValue(), ",");
            invokeArgs[index] = value;
        }

        // 处理request, response参数
        Integer requestIndex = paramNameToIndexMapping.get(HttpServletRequest.class.getSimpleName());
        Integer responseIndex = paramNameToIndexMapping.get(HttpServletResponse.class.getSimpleName());
        invokeArgs[requestIndex] = req;
        invokeArgs[responseIndex] = resp;

        try {
            handler.getMethod().invoke(handler.getControllerObj(), invokeArgs);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }


    /**
     * 权限校验
     * - 解析MySecurity注解
     * - 与权限username进行比对
     */
    private Boolean permissionCheck(Handler handler, String username) {
        Class<?> clazz = handler.getControllerObj().getClass();
        List<String> authorizedValue = new ArrayList<>();
        // 注意authorizedValue解析顺序
        if (clazz.isAnnotationPresent(MySecurity.class)) {
            MySecurity annotation = clazz.getAnnotation(MySecurity.class);
            authorizedValue = Arrays.asList(annotation.value());
        }
        Method method = handler.getMethod();
        if (method.isAnnotationPresent(MySecurity.class)) {
            MySecurity annotation = method.getAnnotation(MySecurity.class);
            authorizedValue = Arrays.asList(annotation.value());
        }

        // 未配置有效@Security, 放行
        if (authorizedValue.size() == 0) {
            return true;
        }

        return authorizedValue.contains(username);
    }
}
