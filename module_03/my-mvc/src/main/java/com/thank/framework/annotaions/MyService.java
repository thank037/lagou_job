package com.thank.framework.annotaions;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/10 18:41
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyService {

    String value() default "";
}
