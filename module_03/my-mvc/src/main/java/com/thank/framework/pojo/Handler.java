package com.thank.framework.pojo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * description: 封装handler方法相关的信息
 *
 * @author thank
 * 2021/5/10 20:08
 */
public class Handler {

    /**
     * invoke方法的handle对象
     */
    private Object controllerObj;

    private Method method;

    /**
     * url, 本身是个字符串, 这里为了增强使用url匹配用pattern
     */
    private Pattern urlPattern;

    /**
     * key: 参数名称, value: 参数在方法中的顺序
     * e.g. {"name", 2}
     */
    private Map<String, Integer> paramNameToIndexMapping = new HashMap<>();

    public Handler() {
    }

    public Handler(Object controllerObj, Method method, Pattern urlPattern) {
        this.controllerObj = controllerObj;
        this.method = method;
        this.urlPattern = urlPattern;
    }

    public Object getControllerObj() {
        return controllerObj;
    }

    public void setControllerObj(Object controllerObj) {
        this.controllerObj = controllerObj;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Pattern getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(Pattern urlPattern) {
        this.urlPattern = urlPattern;
    }

    public Map<String, Integer> getParamNameToIndexMapping() {
        return paramNameToIndexMapping;
    }

    public void setParamNameToIndexMapping(Map<String, Integer> paramNameToIndexMapping) {
        this.paramNameToIndexMapping = paramNameToIndexMapping;
    }
}
