package com.thank.framework.annotaions;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/10 18:39
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAutowired {

    String value() default "";
}
