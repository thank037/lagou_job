package com.thank.framework.annotaions;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/10 18:40
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyRequestMapping {

    String value() default "";
}
