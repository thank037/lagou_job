package com.thank.initializer;

import com.thank.config.AppConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

/**
 * description: 无XML配置初始化Spring上下文和MVC DispatcherServlet
 * 被 {@link MySpringServletContainerInitializer#onStartup(Set, ServletContext)} 调用
 *
 * @author thank
 * 2021/5/14 19:55
 */
public class MyWebApplicationInitializer implements WebApplicationInitializer {

    /**
     * 初始化Spring & 初始化MVC DispatcherServlet
     * 官方文档: https://docs.spring.io/spring-framework/docs/5.0.8.RELEASE/spring-framework-reference/web.html#mvc-servlet
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        // Load Spring web application configuration
        //通过注解的方式初始化Spring的上下文
        AnnotationConfigWebApplicationContext ac = new AnnotationConfigWebApplicationContext();
        //注册spring的配置类（替代传统项目中xml的configuration）
        ac.register(AppConfig.class);
        ac.refresh();

        // Create and register the DispatcherServlet
        //基于java代码的方式初始化DispatcherServlet
        DispatcherServlet servlet = new DispatcherServlet(ac);
        String servletName = "my-mvc";
        if (servletContext.getServletRegistration(servletName) == null) {
            ServletRegistration.Dynamic registration = servletContext.addServlet(servletName, servlet);
            registration.setLoadOnStartup(1);
            registration.addMapping("/app/*");
        }

    }
}
