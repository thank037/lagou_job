package com.thank.initializer;

import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HandlesTypes;
import java.lang.reflect.Modifier;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * description: 当实现了Servlet3.0规范的容器（比如Tomcat7及以上版本）启动时，
 * 会通过SPI扩展机制自动扫描所有jar包里META-INF/services/javax.servlet.ServletContainerInitializer
 * 文件中指定的全路径类（该类需实现ServletContainerInitializer接口），并实例化该类，并回调类中的onStartup方法。
 *
 * @see org.springframework.web.SpringServletContainerInitializer
 * @see HandlesTypes: 可以将指定类,实现传入onStartup的参数集合中, 这里指定为WebApplicationInitializer实现

 * @author thank
 * 2021/5/14 19:57
 */
@HandlesTypes(WebApplicationInitializer.class)
public class MySpringServletContainerInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> waiClassSet, ServletContext servletContext) throws ServletException {

        if (waiClassSet == null || waiClassSet.isEmpty()) {
            System.out.println(" No Spring WebApplicationInitializer types detected on classpath ");
            return;
        }

        System.out.println("====> MySpringServletContainerInitializer --> onStartup()");

        List<WebApplicationInitializer> initializers = new LinkedList<>();

        for (Class<?> waiClass : waiClassSet) {
            // Be defensive: Some servlet containers provide us with invalid classes,
            // no matter what @HandlesTypes says...
            if (!waiClass.isInterface() && !Modifier.isAbstract(waiClass.getModifiers()) &&
                    WebApplicationInitializer.class.isAssignableFrom(waiClass)) {
                try {
                    initializers.add(
                            (WebApplicationInitializer) ReflectionUtils.accessibleConstructor(waiClass).newInstance()
                    );
                } catch (Throwable ex) {
                    throw new ServletException("Failed to instantiate WebApplicationInitializer class", ex);
                }
            }
        }

        if (initializers.isEmpty()) {
            servletContext.log("No Spring WebApplicationInitializer types detected on classpath");
            return;
        }

        servletContext.log(initializers.size() + " Spring WebApplicationInitializers detected on classpath");

        AnnotationAwareOrderComparator.sort(initializers);
        for (WebApplicationInitializer initializer : initializers) {
            initializer.onStartup(servletContext);
        }

    }
}
