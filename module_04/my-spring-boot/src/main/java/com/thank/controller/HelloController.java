package com.thank.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * description: none
 *
 * @author thank
 * 2021/5/14 22:41
 */
@Controller
public class HelloController {

    @RequestMapping("/test")
    @ResponseBody
    public Object test() {
        System.out.println("Hello, My spring boot");
        return ResponseEntity.ok("Hello, My spring boot");
    }
}
