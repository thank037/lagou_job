package com.thank;

import org.apache.catalina.LifecycleException;

import javax.servlet.ServletException;

/**
 * description: 启动引导类
 *
 * @author thank
 * 2021/5/14 20:29
 */
public class MyRunBoot {

    public static void main(String[] args) throws ServletException, LifecycleException {
        SpringApplication.run();
    }
}
