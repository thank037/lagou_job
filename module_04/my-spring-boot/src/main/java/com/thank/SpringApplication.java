package com.thank;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.springframework.util.StopWatch;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * description: none
 *
 * @author thank
 * 2021/5/14 19:52
 */
public class SpringApplication {

    private static int PORT;

    private static String CONTEXT_PATH;

    static {
        InputStream resource = SpringApplication.class.getClassLoader().getResourceAsStream("application.properties");
        Properties properties = new Properties();
        try {
            properties.load(resource);
            PORT = Integer.parseInt(properties.get("server.port").toString());
            CONTEXT_PATH = String.valueOf(properties.get("context.path"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 完成Tomcat的创建与启动
     */
    public static void run() throws LifecycleException, ServletException {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Tomcat tomcat = new Tomcat();
        Connector connector = new Connector();

        connector.setPort(PORT);
        // 注意: web环境
        tomcat.addWebapp(CONTEXT_PATH, "E:\\doc");
        tomcat.getService().addConnector(connector);

        tomcat.start();
        stopWatch.stop();

        System.out.printf("Tomcat started on port(s): %s (http) with context path '%s'%n", PORT, CONTEXT_PATH);
        System.out.printf("Started Application in %s seconds %n", (stopWatch.getTotalTimeMillis() / 1000));

        // 启动后进入wait状态
        tomcat.getServer().await();
    }
}
