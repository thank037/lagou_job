package com.thank.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * description: 配置类, 扫描
 *
 * @author thank
 * 2021/5/14 20:05
 */
@Configuration
@ComponentScan("com.thank")
public class AppConfig {

    public AppConfig() {
        System.out.println("=============== AppConfig component scan ===============");
    }

}
