目录: 
- MyBatis基本应用: 增删改查
- MyBatis关系映射查询: 一对一, 一对多, 多对多
- MyBatis注解开发方式
- MyBatis缓存
- MyBatis插件: 自定义插件, 第三方插件(PageHelper, 通用Mapper)
 


总结:

常用的两种开发方式: 
- Mapper接口 + `*Mapper.xml` 配置方式
- Mapper接口 + 接口内注解方式
> 这两种方式都是用了接口代理, 遵循命名约定, 无需硬编码statementId

MyBatis核心配置文件 `sqlMapConfig.xml` (只是约定俗成叫这个名字, 不叫这个也可以)

开发过程中, 注意区分两种方式在核心配置文件中的读取规则, 不然会经常遇到: `BindingException`

例如: `Type interface com.thank.dao.UserDao is not known to the MapperRegistry.`

### Mapper接口 + mapper.xml方式

如果使用的是Mapper.xml配置SQL方式, mapper引入可以是这样:

```xml
<mappers>
    <mapper resource="UserMapper.xml"/>
    <mapper resource="OrderMapper.xml"/>
</mapper>
```

除了单独引入外, 还支持批量将某个路径下的`mapper.xml`文件进行引入:

```xml
<package name="com.thank.dao"/>
```
此时resources目录下也需要有这个目录并且`mapper.xml`文件在这个目录下, 即`*mapper.xml`文件与Mapper接口需要同包同名

### Mapper接口 + 接口内注解方式

因为此时可能就没有`mapper.xml`文件了, 只需要注册这个Mapper接口即可: 
```xml
<mapper class="com.thank.dao.UserMapper"/>
```
同样可以写成批量的方式: 
```xml
<package name="com.thank.dao"/>
```
除此之外, 如果此时有`mapper.xml`文件, 那么如果`<mapper>`标签中的`namespace`如果包含某个Mapper接口, 那么它也会被自动注册


