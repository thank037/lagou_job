package com.thank;

import com.thank.dao.UserDao;
import com.thank.io.Resources;
import com.thank.pojo.User;
import com.thank.session.SqlSession;
import com.thank.session.SqlSessionFactory;
import com.thank.session.SqlSessionFactoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * description: 请完善自定义持久层框架IPersistence，在现有代码基础上添加、修改及删除功能。【需要采用getMapper方式】
 *
 * @author thank
 * 2021/5/2 20:24
 */
public class CRUDTest {

    private UserDao userDao;

    @Before
    public void before() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        userDao = sqlSession.getMapper(UserDao.class);
    }

    @Test
    public void testInsert() {
        User user = new User();
        user.setUsername("天");
        user.setPassword("pwd");

        int saved = this.userDao.insert(user);
        Assert.assertEquals(1, saved);
    }

    @Test
    public void testUpdate() throws Exception {
        User user = new User();
        user.setId(12);
        user.setUsername("谢谢3");
        user.setPassword("pwd2");

        int updated = userDao.update(user);
        Assert.assertEquals(1, updated);
    }

    @Test
    public void testDelete() throws Exception {
        User user = new User();
        user.setId(11);

        int deleted = userDao.delete(user);
        Assert.assertEquals(1, deleted);
    }

    @Test
    public void testDeleteById() {
        int deleted = this.userDao.deleteById(10);
        Assert.assertEquals(1, deleted);
    }


    @Test
    public void testFind() throws Exception {
        List<User> userList = userDao.findAll();
        userList.forEach(System.out::println);
    }

}
