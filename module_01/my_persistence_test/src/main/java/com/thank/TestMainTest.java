package com.thank;

import com.thank.dao.UserDao;
import com.thank.dao.UserDaoImpl;
import com.thank.io.Resources;
import com.thank.pojo.User;
import com.thank.session.SqlSession;
import com.thank.session.SqlSessionFactory;
import com.thank.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Test;

import java.io.InputStream;
import java.sql.*;
import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/4/16 15:36
 */
public class TestMainTest {

    /**
     * 传统且原始的JDBC方式, 存在如下问题:
     * 1. 数据库的连接, 创建, 释放频繁造成系统资源利用率不高, 从而影响系统性能
     * 2. sql语句再Java代码中硬编码, 实际中可能会频繁改动, 不易维护
     * 3. preparedStatement向sql语句中设置参数存在硬编码 (修改where语句的时候还得修改代码传参处)
     * 4. 封装对象返回结果时存在硬编码, 不智能
     */
    @Test
    public void jdbcTest() {
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/my_mybatis?characterEncoding=utf-8", "root", "root");
            String sql = "select * from user where id = ? and username = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setObject(1, 1);
            preparedStatement.setObject(2, "小袁");
            resultSet = preparedStatement.executeQuery();
            User user = new User();
            while (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
            }
            System.out.println(user);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 自定义持久层改进
     * 1. 使用数据库连接池(c3p0)管理连接资源
     * 2. 将sql语句抽取到xml配置中, 便于管理
     * 3. 在设置参数和封装结果集时, 使用反射, 内省等技术. 将实体字段与表字段进行自动映射(ORM)
     */
    @Test
    public void test() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();


        List<User> userList = sqlSession.selectList("user.findAll");
        for (User user1 : userList) {
            System.out.println("findAll: " + user1);
        }

        User user = sqlSession.selectOne("user.findByCondition", new User(1, "小袁"));
        System.out.println("findByCondition: " + user);
    }


    /**
     * 使用端使用自定义持久层框架实际使用中, 例如如下:
     * @see UserDaoImpl#findAll()
     * @see UserDaoImpl#findByCondition(User)
     * 虽然解决了直接JDBC的一些问题, 但仍存在如下缺点:
     * 1. 可以看到, DAO实现类中每个方法都有一段重复代码, 操作过程模版重复(加载配置文件, 创建SqlSessionFactory, 生产SqlSession)
     * 2. statmentId是xml中与Java代码与之对应的sql语句唯一标识, 硬编码在了代码里
     */
    @Test
    public void testUse() throws Exception {
        UserDao userDao = new UserDaoImpl();
        List<User> userList = userDao.findAll();
        for (User user : userList) {
            System.out.println("findAll: " + user);
        }

        User user = userDao.findByCondition(new User(1, "小袁"));
        System.out.println("findByCondition: " + user);
    }


    /**
     * 改进
     * 解决{@link #testUse()} 中的问题: 使用代理模式来创建接口的代理对象
     */
    @Test
    public void testUse2() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // userDao是个代理对象
        UserDao userDao = sqlSession.getMapper(UserDao.class);

        // 代理对象userDao, 调用接口中任意方法, 都会执行invoke方法
        List<User> userList = userDao.findAll();
        for (User user : userList) {
            System.out.println("findAll: " + user);
        }

//        User user = userDao.findByCondition(new User(1, "小袁"));
//        System.out.println("findByCondition: " + user);
    }




}