package com.thank.dao;


import com.thank.pojo.User;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.util.List;

/**
 * @author thank
 */
public interface UserDao {

    /**
     * Find all user
     */
    List<User> findAll() throws Exception;

    /**
     * Find user by condition
     * @param user User user = new User(1001, "zhangsan");
     */
    User findByCondition(User user) throws Exception;

    int insert(User user);

    Integer update(User user) throws Exception;

    int delete(User user) throws Exception;

    int deleteById(int id);
}
