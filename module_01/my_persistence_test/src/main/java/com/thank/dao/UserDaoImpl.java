package com.thank.dao;

import com.thank.io.Resources;
import com.thank.pojo.User;
import com.thank.session.SqlSession;
import com.thank.session.SqlSessionFactory;
import com.thank.session.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

public class UserDaoImpl implements UserDao {

    @Override
    public List<User> findAll() throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        return sqlSession.selectList("user.findAll");
    }

    @Override
    public User findByCondition(User user) throws Exception {
        InputStream inputStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = sqlSessionFactory.openSession();
        return sqlSession.selectOne("user.findByCondition", new User(1, "小袁"));
    }

    @Override
    public int insert(User user) {
        return 0;
    }

    @Override
    public Integer update(User user) throws Exception {
        return 0;
    }

    @Override
    public int delete(User user) throws Exception {
        return 0;
    }

    @Override
    public int deleteById(int id) {
        return 0;
    }
}
