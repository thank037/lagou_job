package com.thank;

import java.lang.reflect.Method;
import java.sql.*;

/**
 * description: none
 *
 * @author thank
 * 2021/4/18 13:45
 */
public class Main {


    public static void main(String[] args) throws ClassNotFoundException {
        String paramType = "java.lang.Integer";
        Class<?> classType = getClassType(paramType);
        System.out.println(classType);
        Class<Integer> type = Integer.TYPE;
        System.out.println(int.class.isPrimitive());
        System.out.println(Integer.class.isPrimitive());

    }

    private static Class<?> getClassType(String parameterType) throws ClassNotFoundException {
        return parameterType == null || "".equals(parameterType) ? null : Class.forName(parameterType);

    }


    public static void sum(int a, int b ) {
        Connection connection;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/my_mybatis?characterEncoding=utf-8", "root", "root");
//            String sql = "update user set password = 'xxx' where id = 1";
            String sql = "delete from user where id = 4";
            preparedStatement = connection.prepareStatement(sql);
//            preparedStatement.setObject(1, 1);
//            preparedStatement.setObject(2, "小袁");
            int result = preparedStatement.executeUpdate();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
