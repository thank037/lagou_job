package com.thank.io;

import java.io.InputStream;

/**
 * description: 资源加载类
 * e.g. 加载xml配置文件
 * @author thank
 * 2021/4/16 15:57
 */
public class Resources {

    /**
     * 根据文件路径加载为字节输入流
     */
    public static InputStream getResourceAsStream(String filePath) {
       return Resources.class.getClassLoader().getResourceAsStream(filePath);
    }
}
