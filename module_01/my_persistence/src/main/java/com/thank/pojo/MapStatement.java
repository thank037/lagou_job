package com.thank.pojo;

/**
 * description: 封装sql描述信息
 *
 * @author thank
 * 2021/4/16 15:41
 */
public class MapStatement {

    /**
     * ID标识, 即statementId (*Mapper.xml中的namespace + "." + id)
     */
    private String id;

    /**
     * 返回值类型全路径
     */
    private String resultType;

    /**
     * 参数值类型全路径
     */
    private String parameterType;

    /**
     * sql语句
     */
    private String sql;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
