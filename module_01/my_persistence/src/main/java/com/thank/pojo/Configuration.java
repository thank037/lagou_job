package com.thank.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * description: 封装
 * - 存放数据库基本信息
 * - 存放sql描述信息 {@link MapStatement}
 *
 * @author thank
 * 2021/4/16 15:40
 */
public class Configuration {

    private DataSource dataSource;

    /**
     * key: {@link MapStatement#getId()}
     * value: {@link MapStatement}
     */
    private Map<String, MapStatement> mapStatementMap = new HashMap<>();

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MapStatement> getMapStatementMap() {
        return mapStatementMap;
    }

    public void setMapStatementMap(Map<String, MapStatement> mapStatementMap) {
        this.mapStatementMap = mapStatementMap;
    }
}
