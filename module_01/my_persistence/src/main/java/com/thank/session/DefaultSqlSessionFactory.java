package com.thank.session;

import com.thank.pojo.Configuration;

/**
 * description: none
 *
 * @author thank
 * 2021/4/16 20:32
 */
public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
