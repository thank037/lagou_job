package com.thank.session;

import com.thank.config.XMLConfigBuilder;
import com.thank.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * description: none
 *
 * @author thank
 * 2021/4/16 18:11
 */
public class SqlSessionFactoryBuilder {

    public SqlSessionFactory build(InputStream inputStream) throws PropertyVetoException, DocumentException {

        // 1. 使用dom4j解析配置文件，将解析出来的内容封装到Configuration中
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parse(inputStream);

        // 2. 创建sqlSessionFactory对象：工厂类：生产sqlSession:会话对象
        return new DefaultSqlSessionFactory(configuration);

    }
}
