package com.thank.session;

import com.thank.pojo.Configuration;
import com.thank.pojo.MapStatement;

import java.lang.reflect.*;
import java.util.Collections;
import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/4/16 20:34
 */
public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {
        Executor executor = new SimpleExecutor();
        MapStatement mapStatement = configuration.getMapStatementMap().get(statementId);
        return executor.query(configuration, mapStatement, params);
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<T> objects = this.selectList(statementId, params);
        if (objects == null || objects.size() != 1) {
            throw new RuntimeException("查询结果为空或结果为多个");
        }
        return objects.get(0);
    }

    @Override
    public int update(String statementId, Object... params) throws Exception {
        Executor executor = new SimpleExecutor();
        MapStatement mapStatement = configuration.getMapStatementMap().get(statementId);
        return executor.update(configuration, mapStatement, params);
    }


    /**
     * 使用JDK动态代理为DAO接口生成代理对象, 并返回
     */
    @Override
    public <T> T getMapper(Class<T> clazz) {

        /**
         * getMapper产生的代理对象proxy, 代理对象调用接口中的任何方法, 都会执行这里的invoke方法;
         * @param proxy 当前代理对象的引用
         * @param method 当前被调用方法的引用
         * @param args 调用方法传递的参数
         * @return
         * @throws Throwable
         */
        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{clazz}, (proxy, method, args) -> {
            String className = method.getDeclaringClass().getName();
            String methodName = method.getName();
            // 这里的statementId跟`*Mapper.xml`保持约定: namespace与DAO接口的全路径名保持一直, id与方法名保持一致
            String statementId = className + "." + methodName;
            System.out.println("使用代理对象, 并执行invoke方法");

            // update: 返回值为基本类型 e.g. int, Integer, long...
            boolean isBaseType = Number.class.isAssignableFrom(method.getReturnType()) || method.getReturnType().isPrimitive();
            if (isBaseType) {
                return update(statementId, args);
            }

            // 这里进行泛型类型参数化判断, 如果返回值是带泛型, 就认为是selectList, 反之为selectOne
            if (method.getGenericReturnType() instanceof ParameterizedType) {
                return selectList(statementId, args);
            }
            return selectOne(statementId, args);
        });

        return (T) proxyInstance;
    }
}
