package com.thank.session;

import com.thank.pojo.Configuration;
import com.thank.pojo.MapStatement;

import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/4/18 14:17
 */
public interface Executor {

    <T> List<T> query(Configuration configuration, MapStatement mapStatement, Object... params) throws Exception;

    int update(Configuration configuration, MapStatement mapStatement, Object... params) throws Exception;
}
