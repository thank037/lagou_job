package com.thank.session;

import com.thank.pojo.Configuration;

/**
 * @author Administrator
 */
public interface SqlSessionFactory {

    SqlSession openSession();
}
