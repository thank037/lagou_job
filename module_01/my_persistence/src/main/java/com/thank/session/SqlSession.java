package com.thank.session;

import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/4/16 18:15
 */
public interface SqlSession {

    /**
     * 查询列表
     */
    <E> List<E> selectList(String statementId, Object... params) throws Exception;

    /**
     * 查询单个
     */
    <T> T selectOne(String statementId, Object... params) throws Exception;

    int update(String statementId, Object... params) throws Exception;

    /**
     * 为DAO接口生成代理实现类
     */
    <T> T getMapper(Class<T> clazz);
}
