package com.thank.session;

import com.thank.config.BoundSql;
import com.thank.pojo.Configuration;
import com.thank.pojo.MapStatement;
import com.thank.utils.GenericTokenParser;
import com.thank.utils.ParameterMapping;
import com.thank.utils.ParameterMappingTokenHandler;

import javax.sql.DataSource;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/4/18 14:17
 */
public class SimpleExecutor implements Executor {


    @Override
    public <T> List<T> query(Configuration configuration, MapStatement mapStatement, Object... params) throws Exception {

        // 1. 注册驱动, 获取连接
        DataSource dataSource = configuration.getDataSource();
        Connection connection = dataSource.getConnection();

        // 2. 获取sql语句并解析出执行sql及参数
        BoundSql boundSql = this.getBoundSql(mapStatement.getSql());

        // 3. 获取预处理对象: preparedStatement
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());

        // 4. 设置参数
        String parameterType = mapStatement.getParameterType();
        Class<?> parameterTypeClass = this.getClassType(parameterType);
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();

        for (int i = 0; i < parameterMappings.size(); i++) {
            ParameterMapping parameterMapping = parameterMappings.get(i);
            String fieldName = parameterMapping.getContent();
            Field field = parameterTypeClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            Object value = field.get(params[0]);
            preparedStatement.setObject(i+1, value);
        }

        // 5. 执行sql
        ResultSet resultSet = preparedStatement.executeQuery();

        // 6. 封装返回结果集
        String resultType = mapStatement.getResultType();
        Class<?> resultTypeClass = this.getClassType(resultType);
        List<Object> resultList = new ArrayList<>();

        while (resultSet.next()) {
            Object resultObj = resultTypeClass.newInstance();
            ResultSetMetaData metaData = resultSet.getMetaData();
            // 列数据
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String columnName = metaData.getColumnName(i);
                Object columnValue = resultSet.getObject(columnName);
                // 使用反射或者内省，根据数据库表和实体的对应关系，完成封装
                PropertyDescriptor descriptor = new PropertyDescriptor(columnName, resultTypeClass);
                Method writeMethod = descriptor.getWriteMethod();
                writeMethod.invoke(resultObj, columnValue);
            }
            resultList.add(resultObj);
        }

        return (List<T>) resultList;
    }

    @Override
    public int update(Configuration configuration, MapStatement mapStatement, Object... params) throws Exception {
        // 1. 注册驱动, 获取连接
        DataSource dataSource = configuration.getDataSource();
        Connection connection = dataSource.getConnection();

        // 2. 获取sql语句并解析出执行sql及参数
        BoundSql boundSql = this.getBoundSql(mapStatement.getSql());

        // 3. 获取预处理对象: preparedStatement
        PreparedStatement preparedStatement = connection.prepareStatement(boundSql.getSqlText());

        // 4. 设置参数
        String parameterType = mapStatement.getParameterType();
        Class<?> parameterTypeClass = this.getClassType(parameterType);
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();

        Object param = params[0];
        if (param instanceof Number || param instanceof String) {
            preparedStatement.setObject(1, param);
        } else {
            for (int i = 0; i < parameterMappings.size(); i++) {
                ParameterMapping parameterMapping = parameterMappings.get(i);
                String fieldName = parameterMapping.getContent();
                Field field = parameterTypeClass.getDeclaredField(fieldName);
                field.setAccessible(true);
                Object value = field.get(param);
                preparedStatement.setObject(i+1, value);
            }
        }



        return preparedStatement.executeUpdate();
    }

    private Class<?> getClassType(String parameterType) throws ClassNotFoundException {
        return parameterType == null || "".equals(parameterType) ? null : Class.forName(parameterType);

    }

    /**
     * 对sql语句进行解析
     * - 解析出JDBC可执行的SQL语句 (将sql中的`#{}`用`?`代替)
     * - 解析出SQL语句中的参数(对象参数) e.g. `#{name}` --> `name`
     * @param sql 原始SQL
     */
    private BoundSql getBoundSql(String sql) {
        ParameterMappingTokenHandler handler = new ParameterMappingTokenHandler();
        GenericTokenParser parser = new GenericTokenParser("#{", "}", handler);
        String sqlText = parser.parse(sql);
        List<ParameterMapping> parameterMappings = handler.getParameterMappings();
        return new BoundSql(sqlText, parameterMappings);
    }
}
