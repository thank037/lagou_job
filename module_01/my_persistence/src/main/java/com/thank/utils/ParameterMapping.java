package com.thank.utils;

public class ParameterMapping {

    /**
     * 就是参数名
     */
    private String content;

    public ParameterMapping(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
