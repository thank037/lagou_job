package com.thank.config;

import com.thank.pojo.Configuration;
import com.thank.pojo.MapStatement;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/4/16 16:31
 */
public class XMLMapperBuilder {

    private Configuration configuration;

    public XMLMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * 解析`*Mapper.xml`信息, 并封装至 {@link #configuration} 中
     * @param inputStream 文件读入的输入流
     */
    public void parseToConfiguration(InputStream inputStream) throws DocumentException {

        Document document = new SAXReader().read(inputStream);
        Element rootElement = document.getRootElement();
        String namespace = rootElement.attributeValue("namespace");

//        List<Element> list = rootElement.selectNodes("//select");
        List<Element> list = rootElement.selectNodes("/mapper/*");
        for (Element element : list) {
            String id = element.attributeValue("id");
            MapStatement mapStatement = new MapStatement();
            mapStatement.setId(id);
            mapStatement.setParameterType(element.attributeValue("parameterType"));
            mapStatement.setResultType(element.attributeValue("resultType"));
            mapStatement.setSql(element.getTextTrim());
            String statementId = namespace + "." + id;
            configuration.getMapStatementMap().put(statementId, mapStatement);
        }
    }
}
