package com.thank.config;

import com.thank.utils.ParameterMapping;

import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/4/18 14:35
 */
public class BoundSql {

    /**
     * 解析后的JDBC 执行sql文本
     */
    private String sqlText;

    /**
     * sql中的参数集合, e.g. [id, name]
     */
    private List<ParameterMapping> parameterMappings;

    public BoundSql(String sqlText, List<ParameterMapping> parameterMappings) {
        this.sqlText = sqlText;
        this.parameterMappings = parameterMappings;
    }

    public String getSqlText() {
        return sqlText;
    }

    public void setSqlText(String sqlText) {
        this.sqlText = sqlText;
    }

    public List<ParameterMapping> getParameterMappings() {
        return parameterMappings;
    }

    public void setParameterMappings(List<ParameterMapping> parameterMappings) {
        this.parameterMappings = parameterMappings;
    }
}
