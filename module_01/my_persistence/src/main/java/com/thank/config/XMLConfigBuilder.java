package com.thank.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.thank.io.Resources;
import com.thank.pojo.Configuration;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * description: 使用dom4j对`sqlMapConfig.xml`内容进行解析, 封装到Configuration中
 *
 * @author thank
 * 2021/4/16 16:17
 */
public class XMLConfigBuilder {

    private Configuration configuration;

    public XMLConfigBuilder() {
        this.configuration = new Configuration();
    }

    /**
     * 解析过程
     *   1. 解析并封装`sqlMapConfig.xml`中的数据库信息至{@link Configuration#getDataSource()}
     *   2. 找到Mapper路径, 调用{@link XMLMapperBuilder#parseToConfiguration(InputStream)}
     *      解析并封装 `*Mapper.xml`中的sql信息至{@link Configuration#getMapStatementMap()}
     * @param inputStream `sqlMapConfig.xml`读入的输入流
     */
    public Configuration parse(InputStream inputStream) throws DocumentException, PropertyVetoException {

        Document document = new SAXReader().read(inputStream);
        Element rootElement = document.getRootElement();
        List<Element> propertyList = rootElement.selectNodes("//property");

        // Data structure such as `Map<String key, String value>`
        Properties properties = new Properties();
        for (Element element : propertyList) {
            String name = element.attributeValue("name");
            String value = element.attributeValue("value");
            properties.setProperty(name, value);
        }

        // 1. 解析并封装`sqlMapConfig.xml`中的数据库信息
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(properties.getProperty("driverClass"));
        dataSource.setJdbcUrl(properties.getProperty("jdbcUrl"));
        dataSource.setUser(properties.getProperty("username"));
        dataSource.setPassword(properties.getProperty("password"));
        configuration.setDataSource(dataSource);

        // 2. 解析并封装 `*Mapper.xml` 中的内容
        List<Element> mapperList = rootElement.selectNodes("//mapper");
        for (Element element : mapperList) {
            String resource = element.attributeValue("resource");
            XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(configuration);
            xmlMapperBuilder.parseToConfiguration(Resources.getResourceAsStream(resource));
        }

        return configuration;
    }
}
