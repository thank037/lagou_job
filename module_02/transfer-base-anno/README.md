
## 作业: 
```aidl
一、编程题

学员自定义@Service、@Autowired、@Transactional注解类，完成基于注解的IOC容器（Bean对象创建及依赖注入维护）和声明式事务控制，
写到转账工程中，并且可以实现转账成功和转账异常时事务回滚

注意考虑以下情况：

 1）注解有无value属性值【@service（value=""） @Repository（value=""）】 

 2）service层是否实现接口的情况【jdk还是cglib】

二、作业资料说明：

1、提供资料：代码工程、验证及讲解视频、SQL脚本。

2、讲解内容包含：题目分析、实现思路、代码讲解。

3、效果视频验证

   1）实现转账成功和转账异常时事务回滚。

   2）展示和讲解自定义@Service、@Autowired、@Transactional注解类。
```

## 作业思路

### 注解开发

自定义以下三类注解:

Component标识:

- MyComponent
- MyService
- MyRepository 

注入标识:

- MyAutowired

事务注解:

- MyTransaction



### Bean工厂类BeanFactory

**主体思路:**

1. 根据配置`component-scan`解析路径下的bean, 通过反射实例化放入内存Storage map中
2. 实例化完成之后, 进行依赖关系/属性注入 (有`@Autowired`的有注入需求)
3. 处理`@MyTransaction`的Bean, 使用动态代理获取增强的事务管理功能的代理对象, 覆盖进Storage map中

**注意考虑:**

处理Bean的ID时, 符合如下优先级原则

    1. 注解中的value值
    2. 接口类名(符合@Autowired的命名规则)
    3. 该类的类名  (类型首字母小谢)

  > 在实例化Bean放入内存Storage map中和注入属性时都需要考虑到
  >
  > 能够处理情况:
  >
  > - 实例化Bean: `@MyService("abc") public class TransferServiceImpl implements TransferService {}`
  > - Bean属性注入: `@MyAutowired private AccountDao xxxDao;`

在处理@MyTransaction标识的Bean时, 产生代理对象覆盖被代理对象

- 获取代理对象时考虑是否实现接口, 动态代理区分jdk和Cglib两种实现 
- 支持识别实现类上和方法上的注解标注