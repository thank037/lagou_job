package com.thank.anno;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/8 14:30
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyAutowired {
}
