package com.thank.anno;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/8 14:29
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@MyComponent
public @interface MyRepository {

    String value() default "";
}
