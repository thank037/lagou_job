package com.thank.anno;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/8 14:29
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MyTransaction {
}
