package com.thank.anno;

import java.lang.annotation.*;

/**
 * description: none
 *
 * @author thank
 * 2021/5/8 14:26
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MyComponent {

    String value() default "";
}
