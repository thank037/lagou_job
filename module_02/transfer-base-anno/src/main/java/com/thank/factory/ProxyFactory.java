package com.thank.factory;

import com.thank.anno.MyAutowired;
import com.thank.anno.MyComponent;
import com.thank.utils.TransactionManager;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * description: 代理对象工厂 用来生产代理对象
 * - 代理对象的增强逻辑是 事务控制: 事务的开启, 提交, 回滚
 * @author thank
 * 2021/5/6 17:28
 */
@MyComponent
public class ProxyFactory {

    /**
     * 从自定义的bean容器中获取TransactionManager
     */
    @MyAutowired
    private TransactionManager transactionManager;

    /**
     * 使用JDK动态代理生成代理对象
     * @param obj 委托对象
     * @return 代理对象
     */
    public Object getJdkProxy(Object obj) {
        return Proxy.newProxyInstance(
                obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new InvocationHandler() {
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        try {
                            // 开始事务 (关闭事务的自动提交)
                            transactionManager.beginTransaction();
                            Object result = method.invoke(obj, args);
                            // 提交事务
                            transactionManager.commit();
                            return result;
                        } catch (Exception e) {
                            e.printStackTrace();
                            transactionManager.rollback();
                            // 抛出异常, 便于上层应用servlet捕获, 进而处理异常
                            throw e;
                        }

                    }
                });
    }


    /**
     * 使用Cglib动态代理生成代理对象
     *  - 类似于JDK动态代理
     *  - 通过实现接口MethodInterceptor能够对各个方法进行拦截增强，类似于JDK动态代理中的InvocationHandler
     * @param obj 委托对象
     * @return 代理对象
     */
    public Object getCglibProxy(Object obj) {
        return Enhancer.create(obj.getClass(), new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                try {
                    // 开始事务 (关闭事务的自动提交)
                    transactionManager.beginTransaction();
                    Object result = method.invoke(obj, objects);
                    // 提交事务
                    transactionManager.commit();
                    return result;
                } catch (Exception e) {
                    e.printStackTrace();
                    transactionManager.rollback();
                    // 抛出异常, 便于上层应用servlet捕获, 进而处理异常
                    throw e;
                }
            }
        });
    }
}
