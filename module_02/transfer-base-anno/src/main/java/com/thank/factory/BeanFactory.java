package com.thank.factory;

import com.alibaba.druid.util.StringUtils;
import com.thank.anno.MyAutowired;
import com.thank.anno.MyComponent;
import com.thank.anno.MyTransaction;
import com.thank.utils.ClassUtil;
import com.thank.utils.MyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * description: 工厂类, 负责生产对象
 * - 根据packageName解析路径下的bean, 通过反射实例化对象放入Storage map中
 * - 实例化完成之后, 进行依赖关系/属性注入 (有@Autowired的有注入需求)
 * - 处理@MyTransaction的Bean, 使用动态代理获取增强的事务管理功能的代理对象, 覆盖进Storage map中
 *   - 考虑是否实现接口, 动态代理区分jdk和Cglib两种实现
 * @author thank
 * 2021/5/5 21:01
 */
public class BeanFactory {

    /**
     * Storage bean id to bean
     */
    public static Map<String, Object> idToBeanMap = new HashMap<>();

    /**
     * 需要扫描的包路径(Component scan)
     */
    private static String PACKAGE_NAME = "";

    // 加载基础配置文件
    static {
        InputStream inputStream = MyUtils.getResourceAsStream("application.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            Object o = properties.get("component-scan");
            PACKAGE_NAME = o.toString();
            System.out.println("Component scan: " + PACKAGE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            // 1. 实例化Bean
            List<Class<?>> classList = ClassUtil.getClasses(PACKAGE_NAME);
            for (Class<?> clazz : classList) {
                if (clazz.isAnnotation() || clazz.isInterface() || !MyUtils.hashAnnotation(clazz, MyComponent.class)) {
                    continue;
                }

                // id优先级原则: 1.注解中的value值  2.接口类名(符合@Autowired的命名规则)  3.该类的类名  (类型首字母小谢)
                String id = MyUtils.getAnnotationValue(clazz);
                if (StringUtils.isEmpty(id)) {
                    id = (clazz.getInterfaces().length > 0)
                            ? MyUtils.lowercaseFirstLetter(clazz.getInterfaces()[0].getSimpleName())
                            : MyUtils.lowercaseFirstLetter(clazz.getSimpleName());
                }

                idToBeanMap.put(id, clazz.newInstance());
            }

            // 2. 实例化完成之后, 进行依赖关系/属性注入 (有@Autowired的有注入需求)
            for (String beanId : idToBeanMap.keySet()) {
                Object obj = idToBeanMap.get(beanId);
                for (Field field : obj.getClass().getDeclaredFields()) {
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(MyAutowired.class)) {
                        String annotationValue = MyUtils.getAnnotationValue(Class.forName(field.getType().getName()));
                        String injectionBeanId = StringUtils.isEmpty(annotationValue) ?
                                MyUtils.lowercaseFirstLetter(field.getType().getSimpleName()): annotationValue;
                        field.set(obj, idToBeanMap.get(injectionBeanId));
                    }
                }
            }

            // 3. 处理@MyTransaction: 产生代理对象覆盖被代理对象 (
            //    - 动态代理区分jdk/cglib)
            //    - 支持识别实现类上和方法上的注解标注
            for (String beanId : idToBeanMap.keySet()) {
                Object originalObj = idToBeanMap.get(beanId);
                Class<?> clazz = originalObj.getClass();
                if (clazz.isAnnotationPresent(MyTransaction.class)) {
                    idToBeanMap.put(beanId, enhanceProxy(originalObj));
                } else {
                    for (Method method : clazz.getDeclaredMethods()) {
                        if (method.isAnnotationPresent(MyTransaction.class)) {
                            idToBeanMap.put(beanId, enhanceProxy(originalObj));
                            break;
                        }
                    }
                }
            }

        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * 生成带有事务管理功能的增强代理
     * @param originalObj 被代理对象
     * @return 代理对象
     */
    private static Object enhanceProxy(Object originalObj) {
        ProxyFactory proxyFactory = (ProxyFactory) idToBeanMap.get("proxyFactory");
        if (originalObj.getClass().getInterfaces().length > 0) {
            return proxyFactory.getJdkProxy(originalObj);
        }
        return proxyFactory.getCglibProxy(originalObj);
    }


    public static Object getBean(String id) {
        return idToBeanMap.get(id);
    }
}
