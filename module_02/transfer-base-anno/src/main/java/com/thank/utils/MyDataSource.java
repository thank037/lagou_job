package com.thank.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.thank.anno.MyComponent;

import javax.sql.DataSource;

/**
 * description: none
 *
 * @author thank
 * 2021/5/8 17:48
 */
@MyComponent(value = "dataSource")
public class MyDataSource {

    public DataSource getDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/module02_spring");
        dataSource.setUsername("root");
        dataSource.setPassword("root");
        return dataSource;
    }
}
