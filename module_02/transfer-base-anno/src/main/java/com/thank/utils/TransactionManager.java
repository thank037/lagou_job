package com.thank.utils;

import com.thank.anno.MyAutowired;
import com.thank.anno.MyComponent;

import java.sql.SQLException;

/**
 * description: 事务管理器
 * - 负责手动事务的开启, 提交, 回滚
 *
 * @author thank
 * 2021/5/6 16:48
 */
@MyComponent
public class TransactionManager {

    @MyAutowired
    private ConnectionUtils connectionUtils;

    /**
     *  开始事务 (关闭事务的自动提交)
     */
    public void beginTransaction() throws SQLException {
        connectionUtils.getCurrentThreadConn().setAutoCommit(false);
    }

    /**
     * 提交事务
     */
    public void commit() throws SQLException {
        connectionUtils.getCurrentThreadConn().commit();
    }

    /**
     * 回滚
     */
    public void rollback() throws SQLException {
        connectionUtils.getCurrentThreadConn().rollback();
    }
}
