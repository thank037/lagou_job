package com.thank.utils;

import com.thank.anno.MyAutowired;
import com.thank.anno.MyComponent;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * description: 获取当前线程数据库连接的工具类
 *
 * @author thank
 * 2021/5/6 1:23
 */
@MyComponent
public class ConnectionUtils {

    @MyAutowired
    private MyDataSource dataSource;

    public MyDataSource getDataSource() {
        return dataSource;
    }

    /**
     * 存储当前线程的数据库连接
     */
    private ThreadLocal<Connection> threadLocal = new ThreadLocal<>();

    /**
     * 获取当前线程的数据库连接
     * - 先判断当前线程中是否有, 如果没有的话就从连接池中获取一个, 然后绑定到当前线程中
     */
    public Connection getCurrentThreadConn () throws SQLException {
        Connection connection = threadLocal.get();
        if (connection == null) {
            connection = dataSource.getDataSource().getConnection();
            threadLocal.set(connection);
        }
        return connection;
    }

}
