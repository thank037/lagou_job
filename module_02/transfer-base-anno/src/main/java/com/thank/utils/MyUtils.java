package com.thank.utils;

import com.thank.anno.MyComponent;
import com.thank.anno.MyRepository;
import com.thank.anno.MyService;

import java.io.InputStream;
import java.lang.annotation.Annotation;

/**
 * description: none
 *
 * @author thank
 * 2021/5/8 16:17
 */
public class MyUtils {

    /**
     * 首字母转小写
     */
    public static String lowercaseFirstLetter(String str) {
        if (str.charAt(0) >= 97) {
            return str;
        }

        return (char) (str.charAt(0) + 32) + str.substring(1);
    }

    /**
     * 判断该class是否有该注解
     */
    public static <A extends Annotation> boolean hashAnnotation(Class<?> clazz, Class<A> annotationClass) {
        if (null != clazz.getAnnotation(annotationClass)) {
            return true;
        }
        for (Annotation annotation : clazz.getAnnotations()) {
            if (!annotation.toString().startsWith("@java.lang")) {
                return hashAnnotation(annotation.annotationType(), annotationClass);
            }
        }
        return false;
    }

    /**
     * 获取Component注解的value值
     */
    public static String getAnnotationValue(Class<?> clazz) {

        MyComponent myComponent = clazz.getAnnotation(MyComponent.class);
        if (myComponent != null) {
            return myComponent.value();
        }

        MyService myService = clazz.getAnnotation(MyService.class);
        if (myService != null) {
            return myService.value();
        }

        MyRepository myRepository = clazz.getAnnotation(MyRepository.class);
        if (myRepository != null) {
            return myRepository.value();
        }
        return null;
    }

    public static InputStream getResourceAsStream(String filePath) {
        return MyUtils.class.getClassLoader().getResourceAsStream(filePath);
    }
}
