package com.thank.dao;

import com.thank.pojo.Account;

/**
 * description: none
 *
 * @author thank
 * 2021/5/5 18:08
 */
public interface AccountDao {

    Account queryByCardNo(String cardNo) throws Exception;

    int updateByCardNo(Account account) throws Exception;
}
