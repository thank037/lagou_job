package com.thank.service;

/**
 * description: none
 *
 * @author thank
 * 2021/5/5 18:18
 */
public interface TransferService {

    void transfer(String fromCardNo, String toCardNo, int money) throws Exception;
}
