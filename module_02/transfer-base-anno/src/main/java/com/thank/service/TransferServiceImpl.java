package com.thank.service;

import com.thank.anno.MyAutowired;
import com.thank.anno.MyService;
import com.thank.anno.MyTransaction;
import com.thank.dao.AccountDao;
import com.thank.pojo.Account;

/**
 * description: none
 *
 * @author thank
 * 2021/5/5 18:18
 */
@MyService
public class TransferServiceImpl implements TransferService {

    @MyAutowired
    private AccountDao xxxDao;

    @MyTransaction
    @Override
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {

        Account fromAccount = this.xxxDao.queryByCardNo(fromCardNo);
        Account toAccount = this.xxxDao.queryByCardNo(toCardNo);

        fromAccount.setMoney(fromAccount.getMoney() - money);
        toAccount.setMoney(toAccount.getMoney() + money);

        this.xxxDao.updateByCardNo(fromAccount);
        // FIXME: 为了测试事务控制, 这里制造异常
        System.out.println("制造异常:" + 2/0);
        this.xxxDao.updateByCardNo(toAccount);

    }
}
