package com.thank;

import com.thank.utils.MyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * description: none
 *
 * @author thank
 * 2021/5/6 0:26
 */
public class MainTest {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {

//        Map<String, Object> map = BeanFactory.idToBeanMap;
//        for (String key : map.keySet()) {
//            System.out.println("id: " + key + ",  bean: " + map.get(key));
//        }

//        System.out.println("-----------------");
//        Class<?> clazz = Class.forName("com.thank.pojo.Result");
//        Object obj = clazz.newInstance();
//        for (Field field : clazz.getDeclaredFields()) {
//            field.setAccessible(true);
//            System.out.println(field.getType().getSimpleName());
//        }
//
//        Class<?> aClass = obj.getClass();
//        Field[] fields = aClass.getDeclaredFields();
//        for (Field field : fields) {
//            field.setAccessible(true);
//            if ("status".equalsIgnoreCase(field.getName())) {
//                field.set(obj, "OK");
//            }
//
//            if (field.isAnnotationPresent(MyAutowired.class)) {
//                System.out.println("---" + field.getName());
//
//            }
//        }
//

        InputStream inputStream = MyUtils.getResourceAsStream("application.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            Object o = properties.get("component-scan");
            System.out.println(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

